output "s02_networking_vpc_id" {
  value = module.networking.vpc_id
}

output "s02_networking_private_subnets_ids" {
  value = module.networking.private_subnets
}

output "s02_networking_public_subnets_ids" {
  value = module.networking.public_subnets
}
