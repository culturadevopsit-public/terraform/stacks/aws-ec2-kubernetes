locals {
  cidr           = "10.0.0.0/16"
  azs            = ["eu-west-1a"]
  public_subnets = ["10.0.1.0/24"]
}

terraform {
  required_version = "~>1.7.1"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.35.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~>3.1.0"
    }
  }

  backend "s3" {
  }
}

provider "aws" {
  profile = var.provider_profile
  region  = var.provider_region
}

module "networking" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.5.3"

  name           = var.project_name
  cidr           = local.cidr
  azs            = local.azs
  public_subnets = local.public_subnets

  
  enable_dns_hostnames  = true
  enable_dns_support    = true

  tags = {
    Project     = var.project_name
    Environment = var.project_environment
    CreatedBy   = var.project_created_by
    Group       = var.project_group
  }

  public_subnet_tags = {
    Project     = var.project_name
    Environment = var.project_environment
    CreatedBy   = var.project_created_by
    Group       = var.project_group
    Subnet      = "Public-${var.project_name}"
  }
}
