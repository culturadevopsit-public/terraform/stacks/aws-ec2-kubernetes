# Proyecto: AWS EC2 Kubernetes con Terraform

## Descripción

Este proyecto proporciona una infraestructura completa para desplegar un clúster de Kubernetes en Amazon Web Services (AWS) utilizando instancias EC2. La infraestructura se define y se gestiona mediante Terraform, lo que permite una implementación fácil, repetible y escalable.

## Requisitos Previos

Antes de comenzar, asegúrate de tener instaladas las siguientes herramientas:

- [Terraform](https://www.terraform.io/downloads.html) >= 1.0.0
- [AWS CLI](https://aws.amazon.com/cli/) configurado con las credenciales adecuadas
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) para gestionar el clúster de Kubernetes

## Estructura del Proyecto

El proyecto está organizado en los siguientes directorios y archivos:

```
aws-ec2-kubernetes-terraform/
├── modules/
│   ├── ec2/
│   │   ├── main.tf
│   │   ├── variables.tf
│   │   └── outputs.tf
│   ├── vpc/
│   │   ├── main.tf
│   │   ├── variables.tf
│   │   └── outputs.tf
│   └── kubernetes/
│       ├── main.tf
│       ├── variables.tf
│       └── outputs.tf
├── main.tf
├── variables.tf
├── outputs.tf
└── README.md
```

- **modules/**: Contiene módulos reutilizables para gestionar recursos específicos como EC2, VPC y Kubernetes.
- **main.tf**: Archivo principal que incluye y configura los módulos.
- **variables.tf**: Define las variables necesarias para personalizar la infraestructura.
- **outputs.tf**: Especifica los valores de salida que se pueden utilizar después de la creación de la infraestructura.

## Configuración

### Variables

El archivo `variables.tf` contiene las variables necesarias para personalizar el despliegue. Asegúrate de definir los valores adecuados para las siguientes variables:

```hcl
variable "aws_region" {
  description = "La región de AWS donde se desplegará la infraestructura."
  default     = "us-west-2"
}

variable "instance_type" {
  description = "El tipo de instancia EC2 para los nodos de Kubernetes."
  default     = "t2.medium"
}

variable "cluster_name" {
  description = "El nombre del clúster de Kubernetes."
  default     = "k8s-cluster"
}
```

### Backend de Terraform

Configura el backend de Terraform para almacenar el estado de forma remota (opcional pero recomendado):

```hcl
terraform {
  backend "s3" {
    bucket = "mi-bucket-terraform"
    key    = "ruta/al/estado/terraform.tfstate"
    region = "us-west-2"
  }
}
```

### Comandos de Terraform

1. **Inicializar el proyecto**

   ```sh
   terraform init
   ```

2. **Planificar la infraestructura**

   ```sh
   terraform plan
   ```

3. **Aplicar la configuración**

   ```sh
   terraform apply
   ```

## Despliegue de Kubernetes

Una vez que la infraestructura esté en funcionamiento, puedes utilizar `kubectl` para gestionar tu clúster de Kubernetes. Asegúrate de configurar `kubectl` para apuntar a tu clúster recién creado:

```sh
aws eks --region us-west-2 update-kubeconfig --name k8s-cluster
```

## Limpieza

Para destruir la infraestructura creada, ejecuta:

```sh
terraform destroy
```

## Contribuciones

Las contribuciones son bienvenidas. Por favor, abre un issue o un pull request en el repositorio.

## Licencia

Este proyecto está bajo la Licencia MIT. Consulta el archivo `LICENSE` para más detalles.

---

¡Gracias por usar nuestro stack de AWS EC2 Kubernetes con Terraform! Si tienes alguna pregunta o problema, no dudes en abrir un issue.