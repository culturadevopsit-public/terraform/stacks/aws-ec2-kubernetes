output "s02_networking_vpc_id" {
  value = data.terraform_remote_state.vpc.outputs.s02_networking_vpc_id
}

output "s03_security_group_ssh_id" {
  value = aws_security_group.all-open.id
}

output "s03_security_group_ssh_arn" {
  value = aws_security_group.all-open.arn
}

output "s03_master_public_ip" {
  value = aws_instance.master.public_ip
}

output "s03_master_public_dns" {
  value = aws_instance.master.public_dns
}

output "s03_workers_private_ip" {
  value = aws_instance.worker.*.private_ip
}

output "s03_private_key_file" {
  value = local_file.private_key_file.filename
}

output "s03_workers_public_ips" {
  value = ["${aws_instance.worker.*.public_ip}"]
}

output "s03_gitlab_token" {
  value = local.GITLAB_API_TOKEN
}
