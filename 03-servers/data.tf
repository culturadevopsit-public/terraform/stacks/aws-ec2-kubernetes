data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "cultureta-ec2k8s-backend"
    key    = "cultureta-ec2k8s-backend-02-networking.tfstate"
    region = "eu-west-1"
  }
}
